﻿# get otx pulses data from subscribed by alienvault api
# made by rom, the baron of internet | tested on windows powershell ise

$indicators = Invoke-RestMethod -Uri "https://otx.alienvault.com/api/v1/pulses/subscribed/?limit=10&page=1" -Headers @{"X-OTX-API-KEY"="KEY OTX HERE!"}

$(foreach($ioc in $indicators.results.indicators) { 
    [PSCustomObject]@{
        Type = $($ioc.type)
        Indicator = $($ioc.indicator)
    }
 } )  | Export-Csv rezults.csv